<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $counts = Category::count();
        if ($counts == 0) {
            Category::create([
                'name' => '分类一',
            ]);
            Category::create([
                'name' => '分类二',
            ]);
            Category::create([
                'name' => '分类三',
            ]);
        }
    }
}
