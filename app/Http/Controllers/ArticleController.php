<?php

namespace App\Http\Controllers;

use App\Article;

class ArticleController extends Controller
{
    private $article;

    public function __construct(Article $article)
    {
        $this->article = $article;
    }

    public function show($articleId)
    {
        $article = $this->article->findOrFail($articleId);
        $replies = $article->replies()->paginate(10);

        return view('article', compact('article', 'replies', 'articleId'));
    }
}
