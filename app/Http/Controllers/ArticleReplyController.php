<?php

namespace App\Http\Controllers;

use App\Article;
use App\Http\Requests\ReplyRequest;
use Auth;

class ArticleReplyController extends Controller
{
    private $article;

    public function __construct(Article $article)
    {
        $this->article = $article;
    }

    public function store($articleId, ReplyRequest $request)
    {
        $request->merge(['article_id' => $articleId]);
        Auth::user()->replies()->create($request->all());

        return redirect()->route('article.show', $articleId);
    }

    public function update($articleId, $replyId, ReplyRequest $request)
    {
        $article = $this->article->findOrFail($articleId)->replies()->findOrFail($replyId)->update($request->all());

        return redirect()->route('article.show', $articleId);
    }

    public function edit($articleId, $replyId)
    {
        $reply = $this->article->findOrFail($articleId)->replies()->findOrFail($replyId);

        return view('article.reply', compact('reply', 'articleId'));
    }

    public function destroy($articleId, $replyId)
    {
        $reply = $this->article->findOrFail($articleId)->replies()->findOrFail($replyId)->delete();

        return redirect()->route('article.show', $articleId);
    }
}
