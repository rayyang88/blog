<?php

namespace App\Http\Controllers;

use Auth;

class UserNotificationController extends Controller
{
    public function index($userId)
    {
        $notifications = Auth::user()->notifications()
            ->where('type', 'App\Notifications\ReplyNotice')->paginate(10);

        return view('notification', compact('notifications'));
    }
}
