<?php

namespace App\Http\Controllers;

use App\Article;

class HomeController extends Controller
{
    private $article;

    public function __construct(Article $article)
    {
        $this->article = $article;
    }

    public function index()
    {
        $articles = $this->article->orderBy('created_at', 'DESC')->paginate(10);

        return view('home', compact('articles'));
    }
}
