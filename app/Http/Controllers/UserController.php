<?php

namespace App\Http\Controllers;

use App\Article;
use App\Http\Requests\UserRequest;
use Auth;

class UserController extends Controller
{
    private $article;

    public function __construct(Article $article)
    {
        $this->article = $article;
    }

    public function edit($userId)
    {
        $user = Auth::user();

        return view('user.form', compact('user'));
    }

    public function update($userId, UserRequest $request)
    {
        $data = $request->input();
        if ($request->hasFile('avatar')) {
            $image    = $request->file('avatar');
            $upload   = public_path() . '/avatar/';
            $filename = Auth::user()->id . '.' . $image->guessExtension();
            $image->move($upload, $filename);
            $path           = '/avatar/' . $filename;
            $data['avatar'] = $path;
        }

        Auth::user()->update($data);

        return redirect()->route('user.edit', Auth::user());
    }
}
