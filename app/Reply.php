<?php

namespace App;

use App\Notifications\ReplyNotice;
use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    protected $fillable = [
        'content',
        'user_id',
        'article_id',
    ];

    public function article()
    {
        return $this->belongsTo(Article::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    protected static function boot()
    {
        parent::boot();

        self::created(function ($reply) {
            $reply->article->user->notify(new ReplyNotice($reply->article->id, $reply->user));
        });
    }
}
