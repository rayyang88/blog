@extends('layout')

@section('header_title')
我的消息
@endsection

@section('content')
<h2>我的消息</h2>
<div>
<ul>
    @foreach($notifications as $notification)
        <li>
            <p><a href="{{ route('article.show', $notification['data']['article_id']) }}">{{ $notification['data']['user']['name'] }}回复了你</p>
        </li>
    @endforeach
</ul>
</div>
<div class="inner">
    {{ $notifications->appends(Request::except('page'))->links() }}
</div>
@endsection
