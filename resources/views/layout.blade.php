<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>多人博客 - @yield('header_title', '')</title>
    </head>
    <body>
        <h1>多人博客</h1>
        <div>
            <ul>
                <li><a href="{{ route('home.index') }}">首页</a></li>
                @if (Auth::check())
                    <li><a href="{{ route('user.article.create', Auth::user()) }}">发表文章</a></li>
                    <li><a href="{{ route('user.article.index', Auth::user()) }}">我的文章</a></li>
                    <li><a href="{{ route('user.notification.index', Auth::user()) }}">我的消息</a></li>
                    <li><a href="{{ route('user.edit', Auth::user()) }}">我的资料</a></li>
                    <li><a href="{{ route('logout') }}">登出</a></li>
                @else
                    <li><a href="{{ route('login') }}">登录</a></li>
                    <li><a href="{{ route('register') }}">注册</a></li>
                @endif
            </ul>
        </div>
        <div class="content">
            @yield('content')
        </div>
    </body>
</html>
