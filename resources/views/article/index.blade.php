@extends('layout')

@section('header_title')
我的文章
@endsection

@section('content')
<h2>文章列表</h2>
<div>
<ul>
    @foreach($articles as $article)
        <li>
            <h3>{{ $article->title }}</h3>
            <p>{{ $article->created_at }}</p>
            <p><a href="{{ route('user.article.edit', [$userId, $article->id]) }}">编辑</a></p>
            {!! Form::open(['route' => ['user.article.destroy', $userId, $article], 'method' => 'DELETE']) !!}
            <input type="submit" value="删除">
            {!! Form::close() !!}
        </li>
    @endforeach
</ul>
</div>
<div class="inner">
    {{ $articles->appends(Request::except('page'))->links() }}
</div>
@endsection
