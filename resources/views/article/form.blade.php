@extends('layout')

@section('header_title')
$header_title
@endsection

@section('content')
<h2>{{ $header_title }}</h2>
<div>
    <div>
        @if (Route::currentRouteName() === 'user.article.create')
            {!! Form::open(['route' => ['user.article.store', $userId], 'method' => 'POST']) !!}
        @else
            {!! Form::Model($article, ['route' => ['user.article.update', $userId, $article], 'method' => 'PUT']) !!}
        @endif
        <div>
            {{ Form::label('title', '标题') }}
            <div>
                {!! Form::text('title', null, ['placeholder' => '请填写百科知识标题']); !!}
                {!! $errors->first('title', '<div>:message</div>') !!}
            </div>
        </div>
        <div>
            {{ Form::label('category_id', '分类') }}
            <div>
                {!! Form::select('category_id', $categories, null, ['placeholder' => '请选择分类']); !!}
                {!! $errors->first('category_id', '<div>:message</div>') !!}
            </div>
        </div>
        <div>
            {{ Form::label('content', '详细内容') }}
            <div>
                {!! Form::textarea('content', null, ['placeholder' => '请填详细内容']); !!}
                {!! $errors->first('content', '<div>:message</div>') !!}
            </div>
        </div>
    </div>
    <div>
        @if (Route::currentRouteName() === 'user.article.create')
            <input type="submit" value="创建">
        @else
            <input type="submit" value="更新">
        @endif
    </div>
    {!! Form::close() !!}
</div>
@endsection
