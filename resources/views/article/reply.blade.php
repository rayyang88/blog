@extends('layout')

@section('header_title')
编辑回复
@endsection

@section('content')
<h2>编辑回复</h2>
<div>
    <div>
        {!! Form::Model($reply, ['route' => ['article.reply.update', $articleId, $reply], 'method' => 'PUT']) !!}
        <div>
            <div>
                {!! Form::textarea('content', null, ['placeholder' => '请输入回复详情']); !!}
                {!! $errors->first('content', '<div>:message</div>') !!}
            </div>
        </div>
    </div>
    <div>
        <input type="submit" value="更新">
    </div>
    {!! Form::close() !!}
</div>
@endsection
