@extends('layout')

@section('header_title')
用户登录
@endsection

@section('content')
<h2>用户登录</h2>
<div>
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
<form action="{{ route('login.post') }}" method="POST">
    <label>邮箱</label>
    <input type="email" name="email">
    <label>密码</label>
    <input type="password" name="password">
    {{ csrf_field() }}
    <button type="submit">提交</button>
</form>
@endsection
