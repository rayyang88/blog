@extends('layout')

@section('header_title')
用户注册
@endsection

@section('content')
<h2>用户注册</h2>
<div>
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
<form action="{{ route('register.post') }}" method="POST">
    <label>用户名</label>
    <input type="text" name="name">
    <label>邮箱</label>
    <input type="email" name="email">
    <label>密码</label>
    <input type="password" name="password">
    <label>确认密码</label>
    <input type="password" name="password_confirmation">
    {{ csrf_field() }}
    <button type="submit">提交</button>
</form>
@endsection
