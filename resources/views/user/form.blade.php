@extends('layout')

@section('header_title')
我的资料
@endsection

@section('content')
<h2>我的资料</h2>
<div>
    <div>
        {!! Form::Model($user, ['route' => ['user.update', $user], 'method' => 'PUT', 'files' => true]) !!}
        <div>
            {{ Form::label('name', '用户名') }}
            <div>
                {!! Form::text('name', null, ['placeholder' => '请输入用户名']); !!}
                {!! $errors->first('name', '<div>:message</div>') !!}
            </div>
        </div>
        <div>
            {{ Form::label('avatar', '头像') }}
            <div>
            	<img src="{{ $user->avatar }}" width="100" height="100"">
                {!! Form::file('avatar', null); !!}
                {!! $errors->first('avatar', '<div>:message</div>') !!}
            </div>
        </div>
    </div>
    <div>
        {{ Form::submit('更新') }}
    </div>
    {!! Form::close() !!}
</div>
@endsection
