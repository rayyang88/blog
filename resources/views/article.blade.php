@extends('layout')

@section('header_title')
文章详情
@endsection

@section('content')
<h2>{{ $article->title }}</h2>
<h3>{{ $article->created_at }}</h3>
<div>
{{ $article->content }}
</div>
<div>
<h4>回复列表</h4>
<ul>
	@foreach($replies as $reply)
        <p>{{ $reply->user->name }}</p>
        <p>{{ $reply->created_at }}</p>
        <p>{{ $reply->content }}</p>
        @if(Auth::check() && Auth::user()->id == $reply->user->id)
        	<p><a href="{{ route('article.reply.edit', [$articleId, $reply->id]) }}">编辑</a></p>
        	{!! Form::open(['route' => ['article.reply.destroy', $articleId, $reply->id], 'method' => 'DELETE']) !!}
            <input type="submit" value="删除">
            {!! Form::close() !!}
        @endif
	@endforeach
</ul>
</div>
<div>
@if(Auth::check())
<h4>发表回复</h4>
     {!! Form::open(['route' => ['article.reply.store', $article], 'method' => 'POST']) !!}
        {!! Form::textarea('content', null, ['placeholder' => '请输入回复']); !!}
        {!! $errors->first('content', '<div>:message</div>') !!}
         <input type="submit" value="发表">
    {!! Form::close() !!}
</div>
@else
<p><a href="{{ route('login') }}">登陆</a>后可发表回复</p>
@endif
@endsection
