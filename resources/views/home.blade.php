@extends('layout')

@section('header_title')
首页
@endsection

@section('content')
<h2>文章列表</h2>
<div>
<ul>
    @foreach($articles as $article)
        <li>
            <h3><a href="{{ route('article.show', $article) }}">{{ $article->title }}</a></h3>
            <p><img src="{{ $article->user->avatar }}" width="100" height="100"></p>
            <p>{{ $article->user->name }}</p>
            <p>{{ $article->created_at }}</p>
        </li>
    @endforeach
</ul>
</div>
<div class="inner">
    {{ $articles->appends(Request::except('page'))->links() }}
</div>
@endsection
