<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', 'HomeController@index')->name('home.index');

Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('/register', 'Auth\RegisterController@register')->name('register.post');
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login')->name('login.post');
Route::resource('article', 'ArticleController', ['only' => ['show']]);

Route::group(['middleware' => 'auth'], function () {
    Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
    Route::resource('article.reply', 'ArticleReplyController', ['only' => ['store', 'destroy', 'edit', 'update']]);
    Route::resource('user.article', 'UserArticleController');
    Route::resource('user.notification', 'UserNotificationController', ['only' => ['index']]);
    Route::resource('user', 'UserController');
});
